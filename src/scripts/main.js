
var isWebKit = 'webkitAppearance' in document.documentElement.style,
  // zoom-based scaling causes font sizes and line heights to be calculated differently
  // on the other hand, zoom-based scaling correctly anti-aliases fonts during transforms (no need for layer creation hack)
  woid = require('./bespoke-void'),
  scaleMethod = isWebKit ? 'zoom' : 'transform',
  bespoke = require('bespoke'),
  svg = require('./bespoke-svg'),
  bullets = require('./bespoke-bullets'),
  substeps = require('bespoke-substeps/dom'),
  multimedia = require('bespoke-multimedia'),
  classes = require('bespoke-classes'),
  fullscreen = require('bespoke-fullscreen'),
  hash = require('bespoke-hash'),
  nav = require('bespoke-nav'),
  overview = require('bespoke-overview'),
  scale = require('bespoke-scale'),
  extern = require('bespoke-extern');

bespoke.from({ parent: 'article.deck', slides: 'section' }, [
  classes(),
  nav(),
  fullscreen(),
  (scaleMethod ? scale(scaleMethod) : function(deck) {}),
  overview({ columns: 4 }),
  svg(),
  multimedia(),
  substeps('.bullet'),
//   bullets('.build, .build-items > *:not(.build-items)'),
  hash(),
  extern()
]);
