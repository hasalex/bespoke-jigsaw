// A modified version of bespoke-bullets that supports explicit ordering of steps.
module.exports = function(options) {
  return function(deck) {
    ["object", "embed"].forEach(function(elmtType) {
      var elmts = document.querySelectorAll(elmtType);
      var length = elmts.length;
      for (var i = 0; i < length; i++) {
        var elmt = elmts.item(i);
        if (elmt.type != "image/svg+xml") {
          continue;
        }

        // Sync request :(
        var xhr = new XMLHttpRequest();
        xhr.open("GET", elmt.getAttribute(elmt.nodeName === "OBJECT" ? "data" : "src"), false);
        xhr.send();
        // xhr.onreadystatechange = function() {
          if (xhr.readyState === XMLHttpRequest.DONE 
              && xhr.status === 200) {
            var svgDoc = xhr.responseXML.documentElement;
            svgDoc.setAttribute('width', elmt['width']); 
            svgDoc.setAttribute('height', elmt['height']);
            elmt.replaceWith(svgDoc);
          }
        // };
      }
    });
  };
};
