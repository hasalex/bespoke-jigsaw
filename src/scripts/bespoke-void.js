// A modified version of bespoke-bullets that supports explicit ordering of steps.
module.exports = function(options) {
  return function(deck) {
    deck.on('activate', function(e) {
      console.info('activate', e);
    });

    deck.on('deactivate', function(e) {
      console.info('deactivate', e);
    });

    deck.on('next', function(e) {
      console.info('next', e);
    });

    deck.on('prev', function(e) {
      console.info('prev', e);
    });
    
    deck.on('slide', function(e) {
      console.info('slide', e);
    });
  };
};
