// A modified version of bespoke-bullets that supports explicit ordering of steps.
module.exports = function(options) {
  return function(deck) {
    inlineSvg = function(obj) {
      ["object", "embed"].forEach(function(elmtType) {
        let elmts = obj.querySelectorAll(elmtType);
        let length = elmts.length;
        for (var i = 0; i < length; i++) {
          let elmt = elmts.item(i);
          if (elmt.type != "image/svg+xml") {
            continue;
          }

          let xhr = new XMLHttpRequest();
          xhr.open("GET", elmt.getAttribute(elmt.nodeName === "OBJECT" ? "data" : "src"), true);
          xhr.send();
          xhr.onreadystatechange = function() {
            if (xhr.readyState === XMLHttpRequest.DONE 
                && xhr.status === 200) {
              const svgDoc = xhr.responseXML.documentElement;
              svgDoc.setAttribute('width', elmt['width']); 
              svgDoc.setAttribute('height', elmt['height']);
              elmt.replaceWith(svgDoc);
            }
          };
        }
      });
    }

    inlineSvg(document);
  };
};
